import axios from "axios";
//const API_URL = 'http://localhost:8080/'
const API_URL = 'https://red-u-red-progi-be.herokuapp.com'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
const api = axios.create({
    baseURL: API_URL
})

class OdjelService{

    async getOdjel() {
        let url = "/odjel";
        let response = {success: false}
        try {
            let response2 = await api.get(url);
            response.success = true
            response.data = response2.data
        } catch (err) {
            response.data = "Greška."
        }
        return response
    }


    async getOpis(sifodjel) {
        let url = "/odjel/" + sifodjel;
        let response = {success: false}
        try {
            let response2 = await api.get(url);
            response.success = true
            response.data = response2.data
        } catch (err) {
            response.data = "Greška."
        }
        return response
    }

}

export default new OdjelService()