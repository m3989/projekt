import axios from "axios";
//const API_URL = 'http://localhost:8080/'
const API_URL = 'https://red-u-red-progi-be.herokuapp.com'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
const api = axios.create({
    baseURL: API_URL
})

class AdminService{

    async getAllDjelatnici() {
        let djelatnici;
        let success = false;
        try{
             djelatnici = await api.get("djelatnik/nadisve");
             djelatnici = djelatnici.data;
             success = true;
        }catch(err) {
            djelatnici = "Greska!";
        }
        return {
            djelatnici: djelatnici,
            success: success,
        };
    }

    async otkaz(id, username) {
        //OVO NE RADI
        let response={success : false,}
        try{
            console.log("+" + id + "+++++" + username)
            //iddjelatnik: id,
            //kad stavim id onda 401 inace 200
            let varijabla = await api.post("/admin/otkaz", { username: username, lozinka: null})
            console.log(varijabla)
            response.data = varijabla.data
            response.success = true;
        }catch(err){
              response.data = "Dogodila se greška."
        }
        return response
    }

    async dodajDjelatnika(username, odjel, odjel2){
        //dodaj djelatnika
        //odjel ????
        //OVO RADI NAPRAVI ODJEL
        let response={success : false,}
        try{
            console.log("ODJELI + " +odjel + odjel2);
            let varijabla = await api.post("/admin/regi", {username: username,odjel:odjel,odjel2:odjel2})
            response.data = "Dodali ste djelatnika. "
            response.success = true;
        }catch(err){
            response.data = "Dogodila se greška. "
        }

        return response
    }

}

export default new AdminService()