import axios from "axios";
//const API_URL = 'http://localhost:8080/'
const API_URL = 'https://red-u-red-progi-be.herokuapp.com'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
const api = axios.create({
    baseURL: API_URL
})


class QueueService{

    async getClients(username){
        let url = '/djelatnik/' + username + "/Queue";
        console.log(username)
        let response = {success: false, data: undefined, next: false}
        try{
            console.log("pokusaaaaaaj")
            let response2 = await api.post(url, {username: username,})
            console.log("KAATAAAAAAAAAAAAAAAAAa")
            console.log(response2)
            console.log(response2.data)
            console.log("KAATAAAAAAAAAAAAAAAAAa")
            response.salter = response2.data.salter;
            response.odjelvrijeme1 = response2.data.odjelvrijeme1;
            response.odjelvrijeme2 = response2.data.odjelvrijeme2;
            response.odjelime1 = response2.data.odjelime1;
            response.odjelime2 = response2.data.odjelime2;
            response.success = true;

        }catch(err){
            response.data = "Greska!"
        }

        return response
    }

    async getClientsNext(username){
        let url = '/djelatnik/' + username + "/Queue/Next";
        console.log(username)
        let response = {success: false, data: undefined, next: true}
        try{
            let response2 = await api.post(url, {username: username,})
            console.log("I AM INN NEXT SUCCESSSSSSSSSSSSSSSSSSSSSSS")
            response.salter = response2.data.salter;
            response.odjelvrijeme1 = response2.data.odjelvrijeme1;
            response.odjelvrijeme2 = response2.data.odjelvrijeme2;
            response.odjelime1 = response2.data.odjelime1;
            response.odjelime2 = response2.data.odjelime2;
            response.odjelklijentT = response2.data.odjelklijentT ? response2.data.odjelklijentT: "Nema klijenta"
            response.odjelklijentS = response2.data.odjelklijentS ? response2.data.odjelklijentS: "Nema klijenta"
            response.success = true;
            response.next = true;
            return response;
        }catch(err){
            response.data = "Greska!"
            return response
        }


    }
}

export default new QueueService()