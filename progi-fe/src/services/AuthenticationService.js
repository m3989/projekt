// CHANGE THIS
import axios from "axios";
import { useNavigate } from "react-router-dom";


//const API_URL = 'http://localhost:8080/'
const API_URL = 'https://red-u-red-progi-be.herokuapp.com'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'

const api = axios.create({
    baseURL: API_URL
})

class AuthenticationService {


    async logoutBE() {
        let user = JSON.parse(sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME))
        if(user == null)
            return
        //console.log(user)
        let url = '/djelatnik/' + user.username + "/logout";
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        let response = await api.post(url, {username: user.username})
        console.log(response)
        return response
    }


    isUserLoggedIn() {
        let user = JSON.parse(sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME))
        if (user === null) return false
        return true
    }

    getLoggedInUserName() {
        let user = JSON.parse(sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME));
        if (user === null) return ''
        return user.username
    }

    getLoggedInSalter() {
        let user = JSON.parse(sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME));
        if (user === null) return ''
        //console.log(user.salter)
        return user.salter
    }

    //remove password
    registerSuccessfulLogin(username, salter) {
        //console.log(user.username);
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, JSON.stringify({
            username: username,
            salter: salter,
        }))
    }


    async checkUsernameInDB(username) {
        let response = {
            firstLogin: true,
            success: false,
            existsInDB: false,
        }

        console.log("here")
        try {
            let response2 = await api.post('/djelatnik', {username: username});
            //  console.log(response2)
            response.success = true;
            if (response2.data.username)
                response.existsInDB = true
            if (response2.data.lozinka)
                response.firstLogin = false
        } catch (errs) {
            response.data = 'Greska!'
        }

        return response
    }


    async firstLogin(username, password) {

        let url = '/djelatnik/' + username + "/firstLogin";
        let response = {success: false,};
        try {
            let response2 = await api.post(url, {username: username, lozinka: password});
            console.log(response.data)
            response.data = response2.data;
            response.success = true;
        } catch (err) {
            response.data = "Dogodila se greška.";
        }
        return response;
    }

    async login(username, password) {
        let url = '/djelatnik/' + username + "/classicLogin";
        let response = {
            success: false
        }
        try {
            let response2 = await api.post(url, {username: username, lozinka: password})
            // console.log(response2.data)
            response.success = response2.data
            response.data = !response2.data ? "Niste ispravno unijeli lozinku." : response2.data
        } catch (errs) {
            response.data = 'Login failed'
        }
        return response
    }

    async loginAdmin(username, password) {
        let url = "/admin/prijava";
        let response = {success: false}
        try {
            let response2 = await api.post(url, {username: username, lozinka: password});
            response.success = true
        } catch (err) {
            response.data = "Niste ispravno unijeli podatke."
        }
        return response
    }


}

export default new AuthenticationService()