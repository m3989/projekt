import axios from "axios";
//const API_URL = 'http://localhost:8080/'
const API_URL = 'https://red-u-red-progi-be.herokuapp.com'
export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser'
const api = axios.create({
    baseURL: API_URL
})

class KlijentService  {

    async posaljiOdjel(odjel) {
        console.log(odjel)
        try {
            let response = await api.post("/klijent/red", {odjelIBroj: odjel})
            console.log("I am herrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr")
            console.log("response.data u posalji odjel " + response.data)
            sessionStorage.setItem("salter", response.data.salter)
            sessionStorage.setItem("cookie", response.data.cookie)
            return response.data
        } catch (err) {
            return "Greska"
        }
    }

    async vrijemeCekanja() {
        let salter = sessionStorage.getItem("salter")
        let cookie = sessionStorage.getItem("cookie")


        try {
            console.log("pokusaj")
            let response = await api.post("/djelatnik/vrijeme", {salter: salter, cookie: cookie,})
            console.log("response " + response)
            sessionStorage.setItem("vrijemeKlient" , response.data);
            //write vr cekanja u session storage
            console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa true")

            return true;
        } catch (err) {
            console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa false")
            return false;
        }

    }

    getTimeForClient(){
        return sessionStorage.getItem("vrijemeKlient");
    }
    getCookieForClient(){
        return sessionStorage.getItem("cookie");
    }
    getIdForClient(){
        return sessionStorage.getItem("salter");
    }
    getNotificationTime(){
        return sessionStorage.getItem("notification");
    }

    setSelectedNotification(time){
        sessionStorage.setItem("notification", time);
    }

    removeClient(){
        sessionStorage.removeItem("notification")
        sessionStorage.removeItem("salter")
        sessionStorage.removeItem("vrijemeKlient")
        sessionStorage.removeItem("cookie")
    }

    async refreshTime(){
        let salter = sessionStorage.getItem("salter")
        let cookie = this.getCookieForClient();

        try {
            let response = await api.post("/djelatnik/vrijeme", {salter: salter, cookie: cookie})
            return response.data
        } catch (err) {
            return -1;
        }
    }


}

export default new KlijentService()