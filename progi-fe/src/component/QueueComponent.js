import React, {Component, useEffect, useState} from 'react'
import OdjelService from "../services/OdjelService";
import QueueService from "../services/QueueService";
import NavComponent from "./NavComponent";


class QueueComponent extends Component{


    constructor(props) {
        super(props);
        this.state = {
            username: this.props.id,
            success: undefined,
            hasClients: undefined,
            data: [],
            next: false,
        }
        this.handleNext = (e) => {
            e.preventDefault();
            QueueService.getClientsNext(this.state.username).then( res => {
                console.log("i am clicked next" )
                this.setState({
                    salter : res.salter,
                    odjelvrijeme1: res.odjelvrijeme1,
                    odjelvrijeme2: res.odjelvrijeme2,
                    odjelime1: res.odjelime1,
                    odjelime2: res.odjelime2,
                    odjelklijentT: res.odjelklijentT,
                    odjelklijentS: res.odjelklijentS,
                    success: res.success,
                    next: true,
                })
            });

        }
    }

    componentDidMount() {
        QueueService.getClients(this.state.username).then( res => {

            console.log(res);
                this.setState({
                    salter : res.salter,
                    odjelvrijeme1: res.odjelvrijeme1,
                    odjelvrijeme2: res.odjelvrijeme2,
                    odjelime1: res.odjelime1,
                    odjelime2: res.odjelime2,
                    success: res.success,
                    next: false,
                })

        });
    }

    render() {
        console.log("*****************************"  + this.state.next);
        console.log("+++++++++++++++++++++++++++++"  + this.state.success);
        return(
            <div>
                <NavComponent />
                <p>
                    I am here.
                </p>

                {/*here go clients*/}
                {!this.state.next && this.state.success &&  <p>  Salter: {this.state.salter} </p>}
                {!this.state.next && this.state.success &&  <p>  Odjel: {this.state.odjelime1} Pr vrijeme: {this.state.odjelvrijeme1}  </p>}
                {!this.state.next && this.state.success &&  <p>  Odjel: {this.state.odjelime2} Pr vrijeme: {this.state.odjelvrijeme2}  </p>}


                {this.state.next && this.state.success &&  <p>  Salter: {this.state.salter} </p>}
                {this.state.next && this.state.success &&  <p>  Odjel: {this.state.odjelime1} Pr vrijeme: {this.state.odjelvrijeme1}  </p>}
                {this.state.next && this.state.success &&  <p>  Odjel: {this.state.odjelime2} Pr vrijeme: {this.state.odjelvrijeme2}  </p>}
                {this.state.next && this.state.success &&  <p>  Klijent 1: {this.state.odjelklijentT}   </p>}
                {this.state.next && this.state.success &&  <p>  Klijent 2: {this.state.odjelklijentS}  </p>}



                {!this.state.success && <p>{this.state.data}</p>}

                {/*myb form*/}
                <form action="" onSubmit={this.handleNext}>
                    <input type ="submit" onClick={this.handleNext} value="NEXT" />
                </form>

            </div>
        )
    }
}


export default QueueComponent;
