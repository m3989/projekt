
function validateUserName(username){
    let error;
    if(!username.trim()){
       error = "Username required.";
    }
    return error;
}

function validatePasswordAdmin(password) {
    let error;
    if(!password.trim()) {
        error = "Password required.";
    }
    return error;
}

function validateOdjel(odjel) {
    let error;
    if(!odjel.trim()) {
        error = "Odjel required.";
    }
    return error;
}

function validatePassword(password){
    if(!password){
        return "Password required."
    }else if(password.length <= 6){
        return "Password too short. Must be at least 7 characters long.";
    }
}

function validatePasswordMatch(password, password2){

    if(!password2)
        return "Password2 required. "
    else if(password !== password2)
            return "Passwords don't match.";
}

export  {validateUserName}
export  {validateOdjel}
export {validatePasswordMatch}
export {validatePassword}
export {validatePasswordAdmin}