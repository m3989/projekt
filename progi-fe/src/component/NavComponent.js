import React, {Component} from 'react'
import logPng from '../static/login.png'
import AuthenticationService from "../services/AuthenticationService";

class NavComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hasLoginFailed: true,
        }
    }

    handleClickHamburger = () => {
        //console.log("here")
        const header = document.querySelector("header");

        const hidden = document.querySelector("#hidden")
        if (header.classList.contains("is-active")) {
            header.classList.remove("is-active")
            hidden.style.display = 'none'
        } else {
            header.classList.add("is-active")
            hidden.style.display = 'block'
        }

    }

    render() {
        //console.log(logPng)
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        let username =undefined;
        if(isUserLoggedIn){
             username = AuthenticationService.getLoggedInUserName();
        }

        let salter;

        let adminButton = false;
        if (username === 'admin') {
            adminButton = true;
        }else if( isUserLoggedIn){
             salter = AuthenticationService.getLoggedInSalter();
        }
        return (
            <header className="container container-py">

                <nav className="container container-px  flex jc-sb ai-c">
                    <a href="/">red u red</a>
                    <button id="hamburger" onClick={this.handleClickHamburger}
                            className="header__hamburger hide-for-desktop ">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </nav>
                <div>
                    <div id="hidden">
                        {/*
                        check djelatnik is logged in, session
*/}
                        {!isUserLoggedIn &&
                        <div>
                            <a href="/login" id=""><img src="../static/login.png" alt=""/>Prijava djelatnika</a>
                        </div>
                        }
                        {!isUserLoggedIn &&
                        <div>
                            <a href="/loginAdmin" id=""><img src="../static/login.png" alt=""/>Prijava admina</a>
                        </div>
                        }

                        {isUserLoggedIn && <div>

                            <p>{username}</p>
                            {!adminButton &&
                            <p>Salter: {salter}</p>
                            }
                            <a href="/logout" id=""><img src={logPng} alt=""/>Odjava</a>


                            {adminButton &&

                            <a href="/edit"> Uredi djelatnike </a>

                            }
                        </div>}
                    </div>

                </div>
            </header>
        );
    }
}


export default NavComponent;