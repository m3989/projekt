import React, {Component, useState} from 'react'
import {validateUserName} from "./validateInfo";
import {Navigate} from 'react-router-dom'
import AuthenticationService from "../services/AuthenticationService";
import InvalidComponent from "./InvalidComponent";

class UserNameComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            errors: "",
            hasLoginFailed: false,
            showSuccessMessage: false,
            login: false,
            existsInD: false,
            errorDB: false,
        }
        this.handleChange = (event) => {
            //  console.log("here")
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
            // console.log("---" + this.state);
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();

          //  console.log("I am here")
            let errs = validateUserName(this.state.username)
           // console.log("heree")
            if (!errs) {
                // check if exists in DB
                //if doesnt exist in DB => error
               // console.log("herer")
                let response = await AuthenticationService.checkUsernameInDB(this.state.username);



                if (response.success) {
                    if(!response.existsInDB){
                        this.setState({
                            existsInDB: false,
                            login: false,
                            errors: "Djelatnik ne postoji!"
                        })
                    }
                    if (response.existsInDB && response.firstLogin) {
                        this.setState({
                            existsInDB: true,
                            login: false,
                        })
                    }
                    if (response.existsInDB && !response.firstLogin) {
                        this.setState({
                            existsInDB: true,
                            login: true,
                        })
                    }
                } else {
                    console.log("Greska")
                    this.setState({
                        errors: response.data,
                    })
                }
            } else {
                this.setState(
                    {
                        errors: errs,
                    }
                )
            }

        }
    }

    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        if (isUserLoggedIn) {
            let message = <p> Already logged in. Go to main page <a href='/'>here.</a></p>;
            return <InvalidComponent message={message}/>
        }
        //replace username with id
        let renderValue;
        if (this.state.existsInDB && !this.state.login) {
            renderValue = <Navigate to={{
                pathname: `/prvaPrijava/${this.state.username}`,
                state: {username: this.state.username},
            }}/>;
        } else if (this.state.existsInDB && this.state.login) {
            renderValue = <Navigate to={{
                pathname: `/prijava/${this.state.username}`,
                state: {username: this.state.username},
            }}
            />;

        } else {
            renderValue = (
                <div className="">
                    <section className="container container-px container-py">
                        <form className="korisnik__odabir" onSubmit={this.handleSubmit}>
                            <div className="form-inputs">
                                <label htmlFor="username">Username</label>
                                <input type="text" id="username" name="username" placeholder="Username"
                                       value={this.state.username} onChange={this.handleChange}/>
                            </div>
                            {this.state.errors && <p>{this.state.errors}</p>}

                            <button className="form-input-btn" type="submit">Login</button>
                        </form>
                    </section>
                </div>
            )
        }
        return renderValue;
    }

}

export default UserNameComponent