import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import MainIndexComponent from "./MainIndexComponent";
import NavComponent from "./NavComponent";
import SignUpComponent from "./SignUpComponent";
import UserNameComponent from "./UserNameComponent";
import LoginComponent from "./LogInComponent";
import ClientInfo from "./ClientInfo";
import Wrapper from "./Wrapper";
// import {BrowserRouter,} from "react-router-dom";
import InvalidComponent from './InvalidComponent'
import LogoutComponent from "./LogOut";
import AdminComponent from "./AdminComponent";
import AdminEditComponent from "./AdminEditComponent";
import AdminAddComponent from "./AdminAddComponent";
import QueueComponent from "./QueueComponent";
import WaitingTimeClient from "./WaitingTimeClient";

class InstructorApp extends Component {
    render() {
        let message = <p>Enter your username <a href='/login'>here.</a></p>;
        return (
            <Router>
                <Routes>
                    <Route path="/" element={<NavComponent /> }/>
                    <Route  path="/details/:id"   element={<Wrapper component={SignUpComponent} animate={true} />} />
                    <Route path="/login" element={<UserNameComponent  /> }/>
                    <Route  path="/prvaPrijava/:id"   element={<Wrapper component={SignUpComponent} animate={true} />} />
                    <Route  path="/prijava/:id"   element={<Wrapper component={LoginComponent} animate={true} />} />
                    <Route  path="/prijava"   element={<InvalidComponent message={message}/>} />
                    <Route  path="/prvaPrijava"   element={<InvalidComponent message={message}/>} />
                    <Route  path="/djelatnik"   element={<InvalidComponent message={message}/>} />
                    <Route  path="/djelatnik/:id"   element={<Wrapper component={QueueComponent} animate={true} />} />
                    <Route  path="/logout"   element={<LogoutComponent />} />
                    <Route path="/loginAdmin" element={<AdminComponent  /> }/>
                    <Route path="/edit" element={<AdminEditComponent  /> }/>
                    <Route path="/dodajDjelatnika" element={<AdminAddComponent  /> }/>
                    <Route path="/vrijemeCekanja" element={<WaitingTimeClient />} />
                    <Route path="/clientInfo" element={<ClientInfo />} />
                </Routes>
                <Routes>
                    <Route path="/" element={<MainIndexComponent /> }/>
                </Routes>

            </Router>

        )
    }
}

export default InstructorApp