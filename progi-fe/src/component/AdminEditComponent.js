import React, {Component, useState} from 'react'
import {validateUserName} from "./validateInfo";

import {Navigate} from 'react-router-dom'
import AuthenticationService from "../services/AuthenticationService";
import InvalidComponent from "./InvalidComponent";
import AdminComponent from "./AdminComponent";
import AdminService from "../services/AdminService";
import {Fragment} from "react";
import NavComponent from "./NavComponent";


class DjelatnikRow extends Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (<div >
            <label> {
                this.props.username
            }</label>
        </div>)
    }

}

class AdminEditComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            errors: "",
            password: '',
            success: undefined,
            djelatnici: undefined,
        }
        this.handleChange = (event) => {
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();
            let djelatnici = await AdminService.getAllDjelatnici();
           // console.log(djelatnici.success)
            if (djelatnici.success) {
               // console.log("here")
                let value = [];
                for (let djelatnik in djelatnici.djelatnici) {
                    let obj = {
                        username:  djelatnici.djelatnici[djelatnik].username,
                        id: djelatnici.djelatnici[djelatnik].iddjelatnik,
                    }
                    value.push(obj)
                }
                this.setState({
                    djelatnici: value,
                    success: true,
                })
            } else {
                let value = djelatnici.djelatnici
                this.setState({
                    djelatnici: value,
                    success: false,
                })
            }
        }
    }

    render() {

        let username = AuthenticationService.getLoggedInUserName();
        if (username != "admin") {
            return <InvalidComponent message={"Not allowed."}/>
        }

        let rows;
        if (this.state.djelatnici && this.state.success) {
            rows = []
            for (let djelatnik in this.state.djelatnici) {
                rows.push(<DjelatnikRow key = {this.state.djelatnici[djelatnik].username}
                                        id = {this.state.djelatnici[djelatnik].id}
                                        username={this.state.djelatnici[djelatnik].username}/>)

            }

        }
        if (this.state.djelatnici && !this.state.success)
            rows = this.state.djelatnici;
        return (
            <div className="">
                <NavComponent />
                <section className="container container-px container-py">
                    <a href="/dodajDjelatnika">
                        Dodaj djelatnika
                    </a>
                    <form className="korisnik__odabir" onSubmit={this.handleSubmit}>
                        <button className="form-input-btn" type="submit">Pregled radnika</button>
                    </form>
                    <form>
                        {rows && rows}
                    </form>
                </section>
            </div>
        )
    }

}


export default AdminEditComponent