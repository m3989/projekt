import React, {Component, useState} from 'react'
import {validatePassword, validatePasswordMatch} from "./validateInfo";
import AuthenticationService from "../services/AuthenticationService";
import {Navigate} from "react-router-dom";
import InvalidComponent from "./InvalidComponent";

class SignUpComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: this.props.id,
            password: '',
            password2: '',
            errors: {},
            hasLoginFailed: true,
            showSuccessMessage: false
        }
        this.handleChange = (event) => {
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();
            let err = {}
            err.password = validatePassword(this.state.password)
            err.password2 = validatePasswordMatch(this.state.password, this.state.password2)
            if (!err.password && !err.password2) {
                let response = await AuthenticationService.firstLogin(this.state.username, this.state.password);
                if (response.success) {
                    AuthenticationService.registerSuccessfulLogin(this.state.username, response.data);
                    this.setState({
                        hasLoginFailed: false,
                    })
                } else {
                    this.setState({
                            errors: {message: response.data},
                        })
                }
            } else {
                this.setState(
                    {
                        errors: err,
                    }
                )
            }
        }
    }

    render() {


        if (!this.state.hasLoginFailed)
            return <Navigate to={{
                pathname: `/djelatnik/${this.state.username}`,
                state: {username: this.state.username},
            }}
            />

        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        if (isUserLoggedIn) {
            let message = <p> Already logged in. Go to main page <a href='/'>here.</a></p>;
            return <InvalidComponent message={message}/>
        }

        return (
            <div className="">
                <section className="container container-px container-py">
                    <form className="korisnik__odabir" onSubmit={this.handleSubmit}>
                        {this.state.errors.message && <p>{this.state.errors.message}</p>}
                        <div className="form-inputs">
                            <label htmlFor="password">Password</label>
                            <input type="password" id="password" name="password" placeholder="password"
                                   value={this.state.password} onChange={this.handleChange}/>
                        </div>
                        {this.state.errors.password && <p>{this.state.errors.password}</p>}
                        <div className="form-inputs">
                            <label htmlFor="password2">Password</label>
                            <input type="password" id="password2" name="password2" placeholder="password2"
                                   value={this.state.password2} onChange={this.handleChange}/>
                        </div>
                        {this.state.errors.password2 && <p>{this.state.errors.password2}</p>}
                        <button className="form-input-btn" type="submit">Login</button>
                    </form>
                </section>
            </div>
        );
    }

}

export default SignUpComponent