import React, {Component, useState} from 'react'
import AuthenticationService from "../services/AuthenticationService";
import InvalidComponent from "./InvalidComponent";
import {validateOdjel, validateUserName} from "./validateInfo";
import AdminService from "../services/AdminService";
import NavComponent from "./NavComponent";

class AdminAddComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            errors: "",
            odjel: '',
            odjel2:'',
            success: undefined,
            djelatnici: undefined,
            message: undefined,
        }
        this.handleChange = (event) => {
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();
            let errs1 = validateUserName(this.state.username);
            let errs2 = validateOdjel(this.state.odjel);
            let err3 = validateOdjel(this.state.odjel2);
            if (!errs1 && !errs2) {
               let response =  await AdminService.dodajDjelatnika(this.state.username, this.state.odjel,this.state.odjel2)
                console.log(response.data)
                this.setState({
                    success: true,
                    message: response.data
                })
            } else {
                let errs = errs1 ? errs1 + " " : ""
                errs += errs2 ? errs2 : ""
                errs += err3 ? err3 : ""
                this.setState({
                    errors: errs,
                    success: false,
                })
            }
            //add
        }
    }

    render() {

        let username = AuthenticationService.getLoggedInUserName();
        if (username != "admin") {
            return <InvalidComponent message={"Not allowed."}/>
        }

        return (
            <div className="">
                <NavComponent />
                <section className="container container-px container-py">
                    {this.state.success && <p>{this.state.message}Možete pokušati ponovo ili vratiti se na početnu stranicu <a href ="/">ovdje </a></p>}
                    <form className="korisnik__odabir" onSubmit={this.handleSubmit}>
                        <div className="form-inputs">
                            <label htmlFor="username">Username</label>
                            <input type="text" id="username" name="username" placeholder="Username"
                                   value={this.state.username} onChange={this.handleChange}/>
                            <label htmlFor="password">Odjel</label>
                            <input type="text" id="odjel" name="odjel" placeholder="Odjel"
                                   value={this.state.odjel} onChange={this.handleChange}/>
                            <label htmlFor="password2">Odjel</label>
                            <input type="text" id="odjel2" name="odjel2" placeholder="Odjel2"
                                   value={this.state.odjel2} onChange={this.handleChange}/>
                        </div>
                        {this.state.errors && <p>{this.state.errors}</p>}

                        <button className="form-input-btn" type="submit">Dodaj</button>
                    </form>
                </section>
            </div>

        )
    }
}

export default AdminAddComponent