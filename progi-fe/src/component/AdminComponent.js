import React, {Component, useState} from 'react'
import {validateUserName} from "./validateInfo";
import {validatePasswordAdmin} from "./validateInfo";
import  { Navigate  } from 'react-router-dom'
import AuthenticationService from "../services/AuthenticationService";
import InvalidComponent from "./InvalidComponent";


class AdminComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            errors: "",
            password: '',
            success: false,
        }
        this.handleChange = (event) =>{
            //  console.log("here")
            this.setState(
                {
                    [event.target.name]
                        : event.target.value
                }
            )
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();
            let errsUser = validateUserName(this.state.username)
            let errsPass = validatePasswordAdmin(this.state.password)
            if(!errsUser && !errsPass) {
                let response = await AuthenticationService.loginAdmin(this.state.username, this.state.password);
                if(response.success){
                        this.setState({
                            success: true,
                        })
                        AuthenticationService.registerSuccessfulLogin(this.state.username)
                }else{
                    this.setState({
                        errors: response.data,
                    })
                }
            }else{
                let errors = errsUser ? errsUser + " " : "";
                errors+= errsPass ? errsPass : "";
                this.setState({
                           errors:errors,
                })
            }
        }
    }

    render() {
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        if(isUserLoggedIn){
            let message = <p> Already logged in. Go to main page <a href='/'>here.</a></p>;
            return <InvalidComponent message={message} />
        }
        //replace username with id
        let renderValue;
        if(this.state.success){
            renderValue =  <Navigate to={{
                pathname: '/edit',
            }} /> ;
        }else{
            renderValue = (
                <div className="">
                    <section className="container container-px container-py">
                        <form className="korisnik__odabir"  onSubmit={this.handleSubmit}>
                            <div className="form-inputs">
                                <label htmlFor="username">Username</label>
                                <input type="text" id="username" name="username" placeholder="Username"
                                       value={this.state.username} onChange={this.handleChange}/>
                                <label htmlFor="password">Password</label>
                                <input type="password" id="password" name="password" placeholder="Password"
                                       value={this.state.password} onChange={this.handleChange}/>
                            </div>
                            {this.state.errors && <p>{this.state.errors}</p>}
                            <button className="form-input-btn" type="submit">Login</button>
                        </form>
                    </section>
                </div>
            )
        }
        return renderValue;
    }
}

export default AdminComponent