import React, {Component, useEffect, useState} from 'react'
import AdminService from "../services/AdminService";
import OdjelService from "../services/OdjelService";
import KlijentService from "../services/KlijentService";
import {Navigate} from "react-router-dom";
// import { Link, withRouter } from 'react-router-dom'


// class OdjelRow extends Component {
//     constructor(props) {
//         super(props);
//     }
//
//     render() {
//         console.log(this.state)
//         return ( <option value={this.state.key}>aaa</option>)
//     }
//
// }

// class OdjelSelect extends Component{
//     constructor(props) {
//         super(props);
//
//     }
//
//
//     render() {
//
//        //console.log( this.state.data)
//
//         return (
//
//         )
//     }
//
// }

class MainIndexComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            opis: undefined,
            success: false
        }
        this.handleChange = async (event) => {

            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )

            console.log("hereee")
            //console.log(this.state.odjel)
            console.log("hereee")
            //pozovi fiju get opis Odjela
            let res = await OdjelService.getOpis(event.target.value);
            console.log(res.data)
            this.setState({
                opis: res.data
            })
            //postavi state na pozvano i stavi u form opis odjela
        }

        this.handleSubmit = async (event) => {
            event.preventDefault();
            console.log(event.target)
            console.log(this.state.odjel)
            let res = await KlijentService.posaljiOdjel(this.state.odjel)
            console.log("res u handle submit " + res)
            this.setState({
                success: true
            })
        }
    }

    componentDidMount() {
        OdjelService.getOdjel().then( res => {
            console.log(res)
            this.setState({
                data : res.data,
            })
        });
    }

    render() {
        if(this.state.success) {
            return <Navigate to={{
                pathname: '/vrijemeCekanja'
            }}/>
        }
        return (
            <main>
                <section className="container container-px container-py">
                    <div className="korisnik__odabir" action="">
                        <ol>
                            <li>
                                <form onSubmit={this.handleSubmit}>
                                    <h5>odabir odjela</h5>


                                    <div>
                                        <select name="odjel" id="" onChange={this.handleChange}>
                                            <option valaue={"..."}></option>
                                            {this.state.data.map(odjel => (
                                                <option key={odjel.id} value = {odjel.id}>{odjel.naziv}</option>
                                            ))}
                                        </select>
                                        <p>
                                            {this.state.opis && this.state.opis}
                                        </p>
                                    </div>


                                    <input type="submit"/>
                                </form>
                            </li>
                        </ol>
                        {/*<input type="reset"/>*/}
                        {/*<input type="submit"/>*/}
                    </div>
                </section>

            </main>
        );
    }
}



export default MainIndexComponent;