import React, {Component, useEffect, useState} from 'react'
import AdminService from "../services/AdminService";
import OdjelService from "../services/OdjelService";
import KlijentService from "../services/KlijentService";
import MainIndexComponent from "./MainIndexComponent";
import {Navigate} from "react-router-dom";
// import { Link, withRouter } from 'react-router-dom'

class WaitingTimeClient extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: undefined,
            success: false,
        }
        this.handleChange = (event) => {
            //  console.log("here")
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
            // console.log("---" + this.state);
        }
        this.handleSubmit = async event => {
            event.preventDefault()
            console.log(this.state.vrijeme + "aaaaaaaaaaaaaa")
            if (this.state.vrijeme) {

                let v = await KlijentService.vrijemeCekanja();
                KlijentService.setSelectedNotification(this.state.vrijeme)
                console.log("v " + v)
                if(v == false){
                    this.setState({success: v, data: "Došlo je do greške.Pokušajte ponovo!"})
                }else
                    this.setState({success: v})
                console.log("success " + this.state.success)
            }

        }
    }


    render() {

        if(this.state.success){
            return <Navigate to={{
                pathname: `/clientInfo`,
            }}
            />
        }

        return (
            <main>
                {/*<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>*/}
                {/*<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>*/}
                {/*<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>*/}
                {/*<script>*/}
                {/*    $(".js-example-tags").select2({*/}
                {/*    tags: true*/}
                {/*});*/}
                {/*</script>*/}
                <section className="container container-px container-py">
                    {this.state.data &&  <p>   {this.state.data} </p>}
                    <div className="korisnik__odabir" action="">
                        <form onSubmit={this.handleSubmit}>
                            <h5>odabir obavijesti</h5>
                            <select name="vrijeme" id="" className="form-control js-example-tags" onChange={this.handleChange}>
                                <option value="0">Bez obavijesti</option>
                                <option value="10">10 min</option>
                                <option value="15">15 min</option>
                                <option value="30">30 min</option>
                            </select>
                            <input type="submit"/>
                        </form>
                    </div>
                </section>

            </main>
        );
    }

}

export default WaitingTimeClient;