import React, {Component} from 'react'
import AuthenticationService from "../services/AuthenticationService";
import {Navigate} from "react-router-dom";
import InvalidComponent from "./InvalidComponent";

class LogoutComponent extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        AuthenticationService.logoutBE().then(() => {

        }).catch(() => {

        })
    }

    render() {
        return (<Navigate to='/'/>)

    }
}

export default LogoutComponent