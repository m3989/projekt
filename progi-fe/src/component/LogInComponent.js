import React, {Component, useState} from 'react'
import {validatePassword} from "./validateInfo";
import AuthenticationService from "../services/AuthenticationService";
import {Navigate} from "react-router-dom";
import InvalidComponent from "./InvalidComponent";

class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: this.props.id,
            password: '',
            errors: '',
            hasLoginFailed: true,
            showSuccessMessage: false
        }
        this.handleChange = (event) => {
            this.setState(
                {
                    [event.target.name]: event.target.value
                }
            )
        }
        this.handleSubmit = async (event) => {
            event.preventDefault();
            let errs = validatePassword(this.state.password);

            if (!errs) {
                let response = await AuthenticationService.login(this.state.username, this.state.password);
                if (response.success) {
                   // console.log(response.data)

                    AuthenticationService.registerSuccessfulLogin(this.state.username, response.data);
                    this.setState({
                        hasLoginFailed: false
                    })
                } else {
                    console.log(response.data)
                    this.setState({
                        hasLoginFailed: true,
                        errors: response.data
                    })
                }
            } else {
                this.setState({
                    errors: errs
                })
            }
        }
    }

    render() {


        if (!this.state.hasLoginFailed)
            return <Navigate to={{
                pathname: `/djelatnik/${this.state.username}`,
                state: {username: this.state.username},
            }}
            />

        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        if (isUserLoggedIn) {
            let message = <p> Already logged in. Go to main page <a href='/'>here.</a></p>;
            return <InvalidComponent message={message}/>
        }

        return (
            <div className="">
                <section className="container container-px container-py">
                    <form className="korisnik__odabir" onSubmit={this.handleSubmit}>
                        <div className="form-inputs">
                            <label htmlFor="username">Password</label>
                            <input type="password" id="password" name="password" placeholder="Password"
                                   value={this.state.password} onChange={this.handleChange}/>
                        </div>
                        {this.state.errors && <p>{this.state.errors}</p>}
                        <button className="form-input-btn" type="submit">Login</button>
                    </form>
                </section>
            </div>
        )
    }
}

export default LoginComponent