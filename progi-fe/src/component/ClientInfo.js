import React, {Component, useEffect, useState} from 'react'
import AdminService from "../services/AdminService";
import OdjelService from "../services/OdjelService";
import KlijentService from "../services/KlijentService";
import MainIndexComponent from "./MainIndexComponent";
import {Navigate} from "react-router-dom";
// import { Link, withRouter } from 'react-router-dom'

class ClientInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            vrijeme:  KlijentService.getTimeForClient(),
            salter: KlijentService.getIdForClient(),
            success: false,
            brojac: 0,
            next: false,
            notification: 0,
            end: undefined,
        }
    }
    state = {
        test: true,
        brojac: 0,
    };

    handleSubmit = () => {
        KlijentService.removeClient();
        this.setState({
            end: true,
        })
    }
    componentDidMount() {
        const ONE_MIN = 60000;

        let time = KlijentService.getTimeForClient();
        console.log(time);
        //let salter = KlijentService.getIdForClient();

        let notification = KlijentService.getIdForClient() * 1000;

        if(time <= 0){
            this.setState({
                next: true,
            })
            return;
        }

        if(time - notification <= 0){
            this.setState({
                notification: true,
            })
        }else{

            this.timer = setInterval(
                () =>{
                    let vrijeme;
                    KlijentService.refreshTime().then(res => {
                        vrijeme = res;
                        console.log("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB" + vrijeme)
                        if(vrijeme <= 0){
                            this.setState({
                                next: true,
                                vrijeme: vrijeme,
                            })
                            clearInterval(this.timer);
                        }else if(vrijeme - notification <= 0){
                            this.setState({
                                notification: true,
                                vrijeme: vrijeme,
                            })
                        }else {
                            this.setState({
                                vrijeme: vrijeme,
                            })
                        }
                        }

                    ).catch(res => {

                    })





                } ,
                ONE_MIN,
            );
        }



        //get time from session storage
        //display it to client

        //call function and await answer if <= 0 than stop and tell client he is next inline show button
        //when button clicked he is done then delete from storage
        //if time > 0 then call function again

        ///here
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {

        let pom = false;
        if(this.state.notification){
            this.state.notification = false;
            pom = true;
        }

        if(this.state.end)
            return <Navigate to={{
                pathname: '/'
            }}/>
        return (
            <main>

                    <p>Salter: {this.state.salter}</p>

                {
                    {pom} &&  <p> Notification </p>
                }

                {
                    this.state.next &&  <p> Na redu ste!   </p>
                }
                {
                    this.state.next && <form onSubmit={this.handleSubmit}>
                        <input type="submit" value="Gotov sam" />
                    </form>
                }

                {
                    !this.state.next && <p>
                        U redu ste za dolazak na šalter. Molim pričekajte svoj red!
                    </p>
                }
                <p>
                    Vrijeme do vaseg dolaska na red je samo procjena i to bi trebalo uzeti u obzit.
                </p>
                {this.state.vrijeme &&  <p>  Vrijeme (ms) : {this.state.vrijeme} </p>}

            </main>
        );
    }

}

export default ClientInfo;