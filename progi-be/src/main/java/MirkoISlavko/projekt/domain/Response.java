package MirkoISlavko.projekt.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Response {

    private Integer salter;
    private String odjelklijentT;
    private String odjelklijentS;
    private String odjelime1;
    private  String odjelime2;
    private Long odjelvrijeme1;
    private Long odjelvrijeme2;
    private String cookie;

}
