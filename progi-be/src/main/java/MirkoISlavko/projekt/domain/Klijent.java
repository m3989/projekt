package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Klijent {

    private String cookie;
    private String odjelIBroj;
    private String vrijemeCekanja;




}
