
package MirkoISlavko.projekt.domain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import javax.persistence.*;
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "administrator")
@Entity
public class Administrator {
    @Id
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "username",nullable = false)
    private String username;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "lozinka", nullable = false)
    private String lozinka;

}
