package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.transaction.annotation.Transactional;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Table(name = "radina")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Radina {

    @EmbeddedId
    public RadninaId radninaId;


    @Column(name = "prosjecnovrijeme")
    private Long prosjecnovrijeme;


    @Column(name = "brobradenih")
    private Integer brobradenih;



    @ManyToOne
    @MapsId("sifodjel")
    @JoinColumn(name = "sifodjel")
    private Odjel sifodjel;

    @ManyToOne
    @MapsId("iddjelatnik")
    @JoinColumn(name = "iddjelatnik")
    private Djelatnik iddjelatnik;



}