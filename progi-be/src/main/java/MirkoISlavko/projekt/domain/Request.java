package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Request {

    private String cookie;
    private String salter;
    private String odjel;
    private String vrijemeCekanja;
    private String username;
    private Integer iddjelatnik;
    private String odjel2;



}
