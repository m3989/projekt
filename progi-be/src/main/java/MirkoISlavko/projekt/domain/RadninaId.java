package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RadninaId  implements Serializable {

        @Column(name = "sifodjel")
        private String sifodjel;

        @Column(name = "iddjelatnik")
        private Integer iddjelatnik;

}
