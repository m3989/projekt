package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Table(name = "odjel")
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Odjel {
    @Id
    @Column(name = "sifodjel", nullable = false, length = 1)
    private String id;

    @Column(name = "opis", nullable = false, length = 50)
    private String opis;

    @Column(name = "naziv", nullable = false, length = 50)
    private String naziv;


}