package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DjelatnikInfoVracanje {


    private Integer salter;
    private String trenutni_odjel;
    private String sljedeci_odjel;
    private Long vrijeme_odjel1;
    private Long vrijeme_odjel2;
}
