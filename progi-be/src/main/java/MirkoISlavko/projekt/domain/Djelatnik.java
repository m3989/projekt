package MirkoISlavko.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;

@Table(name = "djelatnik", indexes = {
        @Index(name = "djelatnik_username_key", columnList = "username", unique = true)
})
@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Djelatnik {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddjelatnik", nullable = false)
    private Integer id;

    @Lob
    @Column(name = "lozinka")
    @Type(type = "org.hibernate.type.TextType")
    private String lozinka;

    @Lob
    @Column(name = "username", nullable = false)
    @Type(type = "org.hibernate.type.TextType")
    private String username;



}