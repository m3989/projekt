package MirkoISlavko.projekt.service;

import MirkoISlavko.projekt.domain.Djelatnik;
import MirkoISlavko.projekt.domain.Klijent;
import MirkoISlavko.projekt.domain.Salter;
import MirkoISlavko.projekt.exceptions.AuthException;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Upravlja bazom podataka djelatnika.
 * @see Djelatnik
 */
public interface DjelatnikService {

    /**
     * @param username provjerava postoji li taj username u bazi
     * @throws AuthException ako ne postoji u bazi username
     * @return podatke Djelatnika ako postoji
     */
    Optional<Djelatnik> login(String username);

    String getUsernameOfDjelatnik(Integer id);

    /**
     * Funkcija koja se poziva kada se djelatnik PRVI put prijavljuje u sistem.
     * @param password je string kojeg djelatnik upisuje
     * @throws AuthException ako je string null
     */
    void firstLogin(String username, String password);

    List<Djelatnik> listAll();

     Integer getIdOfDjelatnik(String username);

    /**
     * Funkcija koja se poziva kada se djelatnik prijavljuje u sistem
     * @param password je string kojeg djelatnik upisuje
     *    te se uspoređuje sa passwordom koji je već u bazi podataka
     * @throws AuthException ako se password ne poklapa s onim u bazi
     * @return true ako je autentifikacija uspjesna
     */
    boolean classicLogin(String username, String password);

    void logout(String username);

    Salter giveSalter(String username);

    void makeQueue(String username);

    ConcurrentLinkedQueue<Klijent> getQueueOfDjelatnik(String username);

     String getOdjelOfQueue(String username);

    ConcurrentLinkedQueue addKlijentToQUeue(Klijent klijent, Integer idDjelatnik);

     Integer getDjelatnikofSalter(Integer salterId);
     List<Integer> getDjelatnikOfOdjel(String odjel);

    void putVrijemeInDataBase(long time, String odjel, Integer idDjelatnik);

    Integer getSalterOfDjelatnik(Integer idDjelatnik);
    String getOpisOdjela(String sifOdjel);

    String getSljdeciodjel(Integer idDjelatnik);

    String getOdjeliOfSlijedeciKlijent(Integer iddjelatnik);
    Map<String,Long> getVrijemeOfOdjelZaDjelatnik(Integer iddjelatnik);

}