package MirkoISlavko.projekt.service;

import MirkoISlavko.projekt.domain.Administrator;
import MirkoISlavko.projekt.domain.Djelatnik;
import MirkoISlavko.projekt.domain.Request;
import MirkoISlavko.projekt.exceptions.EntityMissingException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminService {
    /**
     *
     * @param  administrator postoji li taj username u bazi zajedno s lozinkom i dali postoji administrator sa tom kombinacijom lozinki
     *
     * @throws Exception ako ne postoji u bazi ili ako nije dobra kombinacija lozinka i  username
     * @return podatke Administratora ako postoji
     */
    Administrator login(Administrator administrator);
    /**
     * Kreira djelatnika u bazu podataka
     * @param djelatnik objekt sadrzi username
     * @return vraca se objekt djelatnik
     * @throws Exception ako je doslo do greske tj vec posotji user u bazi il ije predan prazan objekt
     */
    Djelatnik putDjelatnikDB(Request djelatnik);

}
