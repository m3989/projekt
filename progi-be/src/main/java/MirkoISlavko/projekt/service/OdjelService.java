package MirkoISlavko.projekt.service;

import MirkoISlavko.projekt.domain.Odjel;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface OdjelService {

    /**
     *
     * @param id trazi odjel po predanom id
     * @return opis odjela kao String
     */

    String findOpis(String id);


    /**
     *
     * @return lista svih odjela koji postoje
     */
    List<Odjel> listAllOdjel();


}
