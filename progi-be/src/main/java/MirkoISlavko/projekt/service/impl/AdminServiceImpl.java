package MirkoISlavko.projekt.service.impl;

import MirkoISlavko.projekt.domain.*;
import MirkoISlavko.projekt.exceptions.AuthException;
import MirkoISlavko.projekt.repo.AdministratorRepository;
import MirkoISlavko.projekt.repo.DjelatnikRepository;
import MirkoISlavko.projekt.repo.OdjelRepository;
import MirkoISlavko.projekt.repo.RadinaRepository;
import MirkoISlavko.projekt.service.AdminService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdministratorRepository AdminRepo;

    @Autowired
    DjelatnikRepository DjelatnikRepo;

    @Autowired
    RadinaRepository radinaRepository;

    @Autowired
    OdjelRepository odjelRepository;

    @Override
    public Administrator login(Administrator administrator) {
        try{
            Assert.notNull(administrator,"Objekt ne postoji");
            Assert.notNull(administrator.getLozinka(),"Ne postoji lozinka");
            Assert.notNull(administrator.getUsername(),"Ne postoji username");
            boolean loz = AdminRepo.existsByLozinka(administrator.getLozinka());
            boolean name = AdminRepo.existsByusername(administrator.getUsername());
            Administrator admin = AdminRepo.findAdministratorByUsernameAndLozinka(administrator.getUsername(), administrator.getLozinka());
            Assert.notNull(admin,"Kombinacija lozinke i usernamea nije dobra!");
            return admin;
        }catch (Exception e) {

            throw new AuthException(e.getMessage());
        }

    }
    @Override
    public Djelatnik putDjelatnikDB(Request request) {
        try {
            Assert.notNull(request, "Djelatnik object must be given");

            Optional<Djelatnik> djelatnikT = DjelatnikRepo.findDjelatnikByUsername(request.getUsername());
            Odjel odjeli = new Odjel();
            Odjel odjeli2 = new Odjel();
            odjeli2 = odjelRepository.getOdjelById(request.getOdjel2());
            odjeli = odjelRepository.getOdjelById(request.getOdjel());
            if(odjeli == null || odjeli2 == null) {
                throw new Exception("Ne postoji odjel tog imena");
            }
            if(!djelatnikT.isEmpty() ) {
                throw  new Exception("Djelatnik vec postoji!");
            }
            Djelatnik djelatnik = new Djelatnik();
            djelatnik.setUsername(request.getUsername());
            DjelatnikRepo.save(djelatnik);
            RadninaId radninaId = new RadninaId();
            radninaId.setIddjelatnik(djelatnik.getId());
            radninaId.setSifodjel(odjeli.getId());
            Radina radina = new Radina();
            radina.setRadninaId(radninaId);
            radina.setIddjelatnik(djelatnik);
            radina.setSifodjel(odjeli);
            radina.setProsjecnovrijeme((long)0000);
            radina.setBrobradenih(0);
            RadninaId radninaId2 = new RadninaId();
            radninaId2.setIddjelatnik(djelatnik.getId());
            radninaId2.setSifodjel(odjeli2.getId());
            Radina radina2 = new Radina();
            radina2.setRadninaId(radninaId2);
            radina2.setIddjelatnik(djelatnik);
            radina2.setSifodjel(odjeli2);
            radina2.setProsjecnovrijeme((long)0000);
            radina2.setBrobradenih(0);
            radinaRepository.save(radina2);
            radinaRepository.save(radina);
            return  djelatnik;

        }catch (Exception e) {
            throw  new AuthException(e.getMessage());
        }
    }
}
