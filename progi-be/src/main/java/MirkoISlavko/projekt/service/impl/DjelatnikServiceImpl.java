package MirkoISlavko.projekt.service.impl;

import MirkoISlavko.projekt.domain.*;
import MirkoISlavko.projekt.exceptions.AuthException;
//import MirkoISlavko.projekt.exceptions.MyDataException;
import MirkoISlavko.projekt.repo.DjelatnikRepository;
import MirkoISlavko.projekt.repo.OdjelRepository;
import MirkoISlavko.projekt.repo.RadinaRepository;
import MirkoISlavko.projekt.repo.SalterRepository;
import MirkoISlavko.projekt.service.DjelatnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Console;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class DjelatnikServiceImpl implements DjelatnikService {



    @Autowired
    RadinaRepository radinaRepository;
    //private static final String USERNAME_FORMAT = "[A-Z, a-z]{10}";


    public HashMap<Integer, ConcurrentLinkedQueue<Klijent>> mapOfQueues = new HashMap<>();

    @Autowired
    private DjelatnikRepository djelatnikRepo;

    @Autowired
    private SalterServiceImpl salterService;

    @Autowired
    private SalterRepository salterRepository;

    @Autowired
    private OdjelRepository odjelRepository;

    @Override
    public Optional<Djelatnik> login(String username) {
        try {
            username = username.replaceAll("=", "");
            Assert.notNull(username,"Username nesmije biti null!");
            return djelatnikRepo.findDjelatnikByUsername(username);
        }catch (Exception e) {
            throw new AuthException(e.getMessage());
        }
    }


    @Override
    public void firstLogin(String username, String password){

        try {
            System.out.println(username  + password);
            Assert.notNull(username, "Username nesmije biti null");
            Assert.notNull(password, "Lozinka nesmije biti null!");
            //zasad je u if-u true dok ne smislim neka ograničenja za password
            if(true) {
                djelatnikRepo.setPassword(username, password);
            }
        } catch (Exception e){
            throw new AuthException(e.getMessage());
        }

    }

    @Override
    public boolean classicLogin(String username, String password) {
        try {
            Assert.notNull(username, "Username nesmije biti null");
            Assert.notNull(password, "Password nesmije biti null");
            System.out.println("LOGIN" + username);
            return djelatnikRepo.getDjelatnikByUsername(username).getLozinka().contentEquals(password);
        }catch (Exception e) {
            throw new AuthException(e.getMessage());
        }

    }

    @Override
    public void logout(String username) {
        System.out.println("LOGOUT" + username);
        salterService.RemoveDjelatnikFromSalter(djelatnikRepo.getDjelatnikByUsername(username).getId());
    }

    @Override
    public Salter giveSalter(String username) {
        return salterService.AddDjelatnikToSalter(djelatnikRepo.getDjelatnikByUsername(username).getId());
    }

    @Override
    public void makeQueue(String username) {
        mapOfQueues.put(djelatnikRepo.getDjelatnikByUsername(username).getId(), new ConcurrentLinkedQueue<Klijent>());
        System.out.println(mapOfQueues);
    }

    @Override
    public String getOdjelOfQueue(String username) {
        ConcurrentLinkedQueue<Klijent> queue = mapOfQueues.get(djelatnikRepo.getDjelatnikByUsername(username).getId());
        Klijent odjel = queue.peek();
        String odj = odjel.getOdjelIBroj();
        queue.poll();
        System.out.println(queue);
        return odj;
    }
    @Override
    public  ConcurrentLinkedQueue<Klijent>  getQueueOfDjelatnik(String username) {
        // vraca queue djealtnika kojeg zatratimo
        //moze vratitit i null ako ne posotji queu za tog djelatnika
        //NOVO vraca string odjela kojeg trazimo
        ConcurrentLinkedQueue<Klijent> queue = mapOfQueues.get(djelatnikRepo.getDjelatnikByUsername(username).getId());
        System.out.println(queue);
        System.out.println(mapOfQueues);
        return queue;

    }

    @Override
    public Integer getIdOfDjelatnik(String username) {
        return djelatnikRepo.getDjelatnikByUsername(username).getId();
    }

    public String getUsernameOfDjelatnik(Integer id) {return  djelatnikRepo.getDjelatnikById(id).getUsername();}


    @Override
    public ConcurrentLinkedQueue addKlijentToQUeue(Klijent klijent,Integer idDjelatnik) {
        System.out.println(" u addtoQueue br djelatnika je " + idDjelatnik);
        ConcurrentLinkedQueue<Klijent> queue = mapOfQueues.get(idDjelatnik);
        queue.add(klijent);
        mapOfQueues.replace(idDjelatnik,queue);
       // System.out.println("DODJAEM DIJELATNIKA U QUEUE");
        //System.out.println("OVO JE SAD TAJ QUEUE " + mapOfQueues.get(idDjelatnik));
        return  null;
    }

    @Override
    public Integer getDjelatnikofSalter(Integer djelatnikId) {
     Salter salter2 = salterRepository.getSalterByIddjelatnik(djelatnikId);
    // Assert.notNull(salter2,"Salter ne psotoji vezan za djelatnika");
     return salter2.getId();
    ///vraca na kojem aslteru se nalazi djelatnik

    }

    @Override
    public List<Integer> getDjelatnikOfOdjel(String odjel) {
        List<Integer> listadjelatnika = new LinkedList<>();
       //vratiti listu djelatnika koji rade na odjleima koji se traze
        Odjel odjel1 = odjelRepository.findOdjelById(odjel);
        Djelatnik djelatnik = djelatnikRepo.getDjelatnikByUsername("peroperic");
        Set<Radina> dj = radinaRepository.findAllBySifodjel(odjel1);
        for(Radina as: dj)  {
            Djelatnik iddj = as.getIddjelatnik();
            listadjelatnika.add(iddj.getId());
        }
        if(listadjelatnika.isEmpty()) {
            return  null;
        }else {
            return listadjelatnika;
        }
    }

    @Override
    public void putVrijemeInDataBase(long time, String odjel, Integer idDjelatnik) {
        Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(idDjelatnik);
        Odjel odjel1 = odjelRepository.getOdjelById(odjel);
        System.out.println("DJELATNIK I ODJEL ZA RADI NA KAJ NAM = VRACA" + djelatnik + odjel1);
        Radina radnina = radinaRepository.getRadinaByIddjelatnikAndAndSifodjel(djelatnik,odjel1);
        Integer brojObradenih = radnina.getBrobradenih();
        radnina.getProsjecnovrijeme();
        Long vrijeme = radnina.getProsjecnovrijeme();
        Long novoVrijeme = (vrijeme + time) / (brojObradenih + 1);
        radinaRepository.updateVrijeme(novoVrijeme, radnina.getSifodjel().getId(), radnina.getIddjelatnik().getId());
        radinaRepository.updateBroj(brojObradenih+1, radnina.getSifodjel().getId(), radnina.getIddjelatnik().getId());
        //radinaRepository.update(novoVrijeme,brojObradenih+1,radnina.getSifodjel().getId(),radnina.getIddjelatnik().getId());


    }

    @Override
    public Integer getSalterOfDjelatnik(Integer idDjelatnik) {
        Salter salter = salterRepository.getSalterByIddjelatnik(idDjelatnik);
        return salter.getId();
    }

    @Override
    public Map<String,Long> getVrijemeOfOdjelZaDjelatnik(Integer iddjelatnik) {
        Map<String,Long> mapa = new HashMap<>();
        Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(iddjelatnik);
        Set<Radina> radina = radinaRepository.findAllByIddjelatnik(djelatnik);
        for(var radi : radina) {
            Odjel odjel = radi.getSifodjel();
            mapa.put(odjel.getOpis(),radi.getProsjecnovrijeme());
        }
        return  mapa;
    }

    @Override
    public String getOdjeliOfSlijedeciKlijent(Integer iddjelatnik) {
        Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(iddjelatnik);
        ConcurrentLinkedQueue<Klijent> queue = getQueueOfDjelatnik(djelatnik.getUsername());
        if(queue.isEmpty()) {
            return null;
        }else {
            Klijent klijent = queue.peek();
            Odjel odjel = odjelRepository.getOdjelById(klijent.getOdjelIBroj());
            return odjel.getOpis();
        }
    }

    @Override
    public String getSljdeciodjel(Integer idDjelatnik) {
        Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(idDjelatnik);
        ConcurrentLinkedQueue<Klijent> queue = getQueueOfDjelatnik(djelatnik.getUsername());
        return null;
    }

    @Override
    public String getOpisOdjela(String sifOdjel) {
        return odjelRepository.getOdjelById(sifOdjel).getOpis();
    }

    @Override
    public List<Djelatnik> listAll() { return djelatnikRepo.findAll(); }

}
