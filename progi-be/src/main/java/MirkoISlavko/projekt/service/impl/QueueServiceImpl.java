package MirkoISlavko.projekt.service.impl;


import MirkoISlavko.projekt.domain.*;
import MirkoISlavko.projekt.repo.DjelatnikRepository;
import MirkoISlavko.projekt.repo.OdjelRepository;
import MirkoISlavko.projekt.repo.RadinaRepository;
import MirkoISlavko.projekt.repo.SalterRepository;
import MirkoISlavko.projekt.service.DjelatnikService;
import MirkoISlavko.projekt.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

@Service
public class QueueServiceImpl implements QueueService {

    public static Long namjanjevrijeme;

    @Autowired
    DjelatnikService djelatnikService;

    @Autowired
    DjelatnikRepository djelatnikRepo;

    @Autowired
    SalterRepository salterRepository;

    @Autowired
    OdjelRepository odjelRepository;

    @Autowired
    RadinaRepository radinaRepository;


    @Override
    public Long getProsjecnovrijeme(Request request) {
        String cookie = request.getCookie();
        String salter = request.getSalter();
        System.out.println("SSSSSSSSSSSSSSSSSSSSSSSSSS" + request.getCookie() + request.getSalter() );
        Integer salter1 = Integer.parseInt(salter);
        Integer iddjelatnik = salterRepository.findDjelatnik(salter1);
        ConcurrentLinkedQueue<Klijent> queue = djelatnikService.getQueueOfDjelatnik(djelatnikRepo.getDjelatnikById(iddjelatnik).getUsername());
        System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE + " + queue);
        if(queue == null || queue.isEmpty()) {
            System.out.println("VRACA NULL JER JE QUEUE NULL" + queue);
            return null;
        }
        List<Klijent> l = new ArrayList(queue);
        int koliko = 0;
        for(var q:l) {
            if(!q.getCookie().equals(cookie)) {
                koliko++;
            } else if(q.getCookie().equals(cookie)) {
                koliko++;
                break;
            }
        }
        List<String> lista = getAllOdjlesOfQueue(queue,koliko); // vraca u listi sve odjele od tog queue
        Long ukupnoProsjVrijeme = (long) 0;
        if(lista == null) {
            System.out.println("VARCA NULL JER JE LISTA NULL" + lista);
            return null;
        }
        for (String odjel : lista) {
            Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(iddjelatnik);
            Odjel odjel1 = odjelRepository.getOdjelById(odjel);
            Radina radina = radinaRepository.getRadinaByIddjelatnikAndAndSifodjel(djelatnik, odjel1);
            if(radina != null)  ukupnoProsjVrijeme += radina.getProsjecnovrijeme();

        }
        return  ukupnoProsjVrijeme;
    }

    @Override
    public List<String> getAllOdjlesOfQueue( ConcurrentLinkedQueue<Klijent> queue,int koliko) {
        // za poslai queue vrati mu listu sa svim odjelima klijenata ,lista.size je duljina queue zapravo

        List<String> odjeli = new LinkedList<>();

        queue.stream().forEach(a -> {odjeli.add(a.getOdjelIBroj());});
        if(!odjeli.isEmpty()) {
            List<String> odjeliKojiNamtrebaju = new LinkedList<>();
            for (int i = 0; i < koliko-1; i++) {
                odjeliKojiNamtrebaju.add(odjeli.get(i));
            }
            return  odjeliKojiNamtrebaju;
        } else {
            return null;
        }
    }
    @Override
    public Integer addToQueue(Klijent klijent) throws Exception {

        List<Integer> sviDjelatnici = djelatnikService.getDjelatnikOfOdjel(klijent.getOdjelIBroj()); // dobimo sve djelatnike koji rade na odjelu
        //djelatnikService.makeQueue(1);
        //sviDjelatnici.add(1);
        //sviDjelatnici.add(2);
        List<Integer> listOfPossibleDjelatnika = new LinkedList<>();  // lista svih djelatnika koji radne trenutno dodjeljen im je salter
        List<Integer> finalList = new LinkedList<>();
        for(Integer id: sviDjelatnici) { // ova for petlja nade sve djelatnike koji radne trenutno
            if(salterRepository.existsByIddjelatnik(id)) {
                listOfPossibleDjelatnika.add(id);
            }
        }
        List<ConcurrentLinkedQueue> listOfPossibleQueue = new LinkedList<>(); // lista svih mogucih queue za djelantike koji rade na tom odjelu
        for( Integer id  : listOfPossibleDjelatnika) {
            ConcurrentLinkedQueue queue = djelatnikService.getQueueOfDjelatnik(djelatnikRepo.getDjelatnikById(id).getUsername());

            if(queue != null) {
                listOfPossibleQueue.add(queue); // popis svih mogucih queue
                finalList.add(id);  //popis svih djelartnika za koje postoji queue,mozda i nepotrebno vijdet cemo
            }
        }
        Integer idDjelatnik = 0;
        if(!finalList.isEmpty()) {
            Integer idDjelatnikaSaNAjmanjimVremenom = 0;
            Long najmanjevrijeme = (long)-1;
            for (Integer id : finalList) {
                ConcurrentLinkedQueue<Klijent> queue = djelatnikService.getQueueOfDjelatnik(djelatnikRepo.getDjelatnikById(id).getUsername());
                Long ukupnoProsjVrijeme = (long)00000; // dok je jos int
                List<String> lista = getAllOdjlesOfQueue(queue,queue.size()); // vraca u listi sve odjele od tog queue
               if(lista != null) {
                   for (String odjel : lista) {
                       Djelatnik djelatnik = djelatnikRepo.getDjelatnikById(id);
                       Odjel odjel1 = odjelRepository.getOdjelById(odjel);
                       Radina radina = radinaRepository.getRadinaByIddjelatnikAndAndSifodjel(djelatnik, odjel1);
                       if (radina != null) ukupnoProsjVrijeme += radina.getProsjecnovrijeme();

                   }
               }

                //System.out.println(ukupnoProsjVrijeme + "ovo je ukupno prosjecno vrijeme za red djelatnika" + id);
                if(najmanjevrijeme == -1) {
                    najmanjevrijeme = ukupnoProsjVrijeme;
                    idDjelatnikaSaNAjmanjimVremenom = id;
                }  else {
                    if(ukupnoProsjVrijeme < najmanjevrijeme) {
                        najmanjevrijeme = ukupnoProsjVrijeme;
                        idDjelatnikaSaNAjmanjimVremenom = id;
                    }

                }

            }
            idDjelatnik = idDjelatnikaSaNAjmanjimVremenom;
            djelatnikService.addKlijentToQUeue(klijent, idDjelatnik);
            Integer s = djelatnikService.getDjelatnikofSalter(idDjelatnik);
            System.out.println(s + " salter plus iddjelatnik " + idDjelatnik);
            return  s;
        }else {
           // System.out.println("NE posotji red sa tim imenom");
            throw new Exception("NE PSOTOJI NI JEDAN MOGUCI RED");

        }


    }

    public  String generateRandomPassword(int len) {
        String chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijk"
                +"lmnopqrstuvwxyz!@#$%&";
        Random rnd = new Random();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(chars.charAt(rnd.nextInt(chars.length())));
        return sb.toString();
    }
}
