package MirkoISlavko.projekt.service.impl;

import MirkoISlavko.projekt.domain.Salter;
import MirkoISlavko.projekt.repo.SalterRepository;
import MirkoISlavko.projekt.service.SalterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalterServiceImpl implements SalterService {

    @Autowired
    private SalterRepository salterRepo;

    @Override
    public Salter AddDjelatnikToSalter(Integer djelatnikId) {
        Salter prazanSalter = salterRepo.findTopByIddjelatnikIsNullOrderById();
        Integer salterId = prazanSalter.getId();
        salterRepo.setDjelatnikToSalter(salterId, djelatnikId);
        return prazanSalter;
    }

    @Override
    public void RemoveDjelatnikFromSalter(Integer djelatnikId) {
        salterRepo.removeDjelatnikFromSalter(djelatnikId);
    }

}
