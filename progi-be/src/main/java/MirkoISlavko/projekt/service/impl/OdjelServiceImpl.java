package MirkoISlavko.projekt.service.impl;

import MirkoISlavko.projekt.repo.OdjelRepository;
import MirkoISlavko.projekt.service.OdjelService;
import MirkoISlavko.projekt.domain.Odjel;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OdjelServiceImpl implements OdjelService {

    @Autowired
    OdjelRepository OdjelRepo;


    @Override
    public String findOpis (String id){
        Odjel odjel = OdjelRepo.findOdjelById(id);      // trazimo odjel po id
        if(odjel == null){                              // ako odjel ne postoji vracamo null
            return null;
        }
        String opis = odjel.getOpis();                  // trazimo opis od odjel
        return opis;
    }

    @Override
    public List<Odjel> listAllOdjel(){
       List<Odjel> list = OdjelRepo.findAll();          // lista svih odjela
       return list;
    }
}
