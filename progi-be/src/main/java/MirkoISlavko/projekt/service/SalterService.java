package MirkoISlavko.projekt.service;

import MirkoISlavko.projekt.domain.Salter;

public interface SalterService {

    Salter AddDjelatnikToSalter(Integer djelatnikId);

    void RemoveDjelatnikFromSalter(Integer salterId);

}
