package MirkoISlavko.projekt.service;

import MirkoISlavko.projekt.domain.Klijent;
import MirkoISlavko.projekt.domain.Request;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;


public interface QueueService  {

    Integer addToQueue(Klijent klijent) throws Exception;

     Long getProsjecnovrijeme(Request request);

    List<String> getAllOdjlesOfQueue(ConcurrentLinkedQueue<Klijent> queue,int koliko);

    String generateRandomPassword(int len);

    //boolean addToDjelatnikovQueue()
}
