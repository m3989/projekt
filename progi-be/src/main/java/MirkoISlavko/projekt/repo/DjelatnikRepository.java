package MirkoISlavko.projekt.repo;

import MirkoISlavko.projekt.domain.Djelatnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public interface DjelatnikRepository extends JpaRepository<Djelatnik, Integer> {

    Optional<Djelatnik> findDjelatnikByUsername(String username);

    Djelatnik getDjelatnikById(Integer id);

    Djelatnik getDjelatnikByUsername(String username);

    @Transactional
    @Modifying
    @Query("UPDATE Djelatnik SET lozinka = :password WHERE username = :username")
    void setPassword(@Param("username") String username, @Param("password") String password);

}