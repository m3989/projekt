package MirkoISlavko.projekt.repo;

import MirkoISlavko.projekt.domain.Administrator;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, String> {
    boolean existsByusername(String username);
    boolean existsByLozinka(String lozinka);

    Administrator findAdministratorByUsernameAndLozinka(String username, String lozinka);


}