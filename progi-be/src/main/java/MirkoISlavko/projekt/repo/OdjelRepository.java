package MirkoISlavko.projekt.repo;

import MirkoISlavko.projekt.domain.Odjel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OdjelRepository extends JpaRepository<Odjel, String> {

    /**
     *
     * @param id trazi Odjel s tim id
     * @return objekt Odjel sa predanim id
     */
    Odjel findOdjelById(String id);

    /**
     *
     * @param naziv trazi Odjel s tim nazivom
     * @return objekt odjel sa tim nazivom
     */
    Odjel getOdjelByNaziv(String naziv);


    Odjel getOdjelById(String s);


}