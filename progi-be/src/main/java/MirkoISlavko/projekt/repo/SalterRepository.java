package MirkoISlavko.projekt.repo;

import MirkoISlavko.projekt.domain.Salter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface SalterRepository extends JpaRepository<Salter, Integer> {

    Salter findTopByIddjelatnikIsNullOrderById();



    boolean existsByIddjelatnik(Integer id);

    Salter getSalterByIddjelatnik(Integer id);

    Salter findSalterByIddjelatnik(Integer id);

    @Transactional
    @Query("SELECT id FROM Salter WHERE iddjelatnik =:iddjelatnik")
    String findSalter(@Param("iddjelatnik") Integer iddjelatnik);

    @Transactional
    @Query("SELECT iddjelatnik FROM Salter WHERE id =:id")
    Integer findDjelatnik(@Param("id") Integer id);

    @Transactional
    @Modifying
    @Query("UPDATE Salter SET iddjelatnik = :djelatnikId WHERE id = :salterId")
    void setDjelatnikToSalter(@Param("salterId") Integer salterId, @Param("djelatnikId") Integer djelatnikId);

    @Transactional
    @Modifying
    @Query("UPDATE Salter SET iddjelatnik = null WHERE iddjelatnik = :djelatnikId")
    void removeDjelatnikFromSalter(@Param("djelatnikId") Integer djelatnikId);

}