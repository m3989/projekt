package MirkoISlavko.projekt.repo;

import MirkoISlavko.projekt.domain.Djelatnik;
import MirkoISlavko.projekt.domain.Odjel;
import MirkoISlavko.projekt.domain.Radina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RadinaRepository extends JpaRepository<Radina, String> {

    Set<Radina> findAllBySifodjel(Odjel odjel);
    Set<Radina> findAllByIddjelatnik(Djelatnik djelatnik);
    boolean existsRadinasByIddjelatnik(Djelatnik a);
    boolean existsRadinasBySifodjel(Odjel a);

    Radina getRadinaByIddjelatnikAndAndSifodjel(Djelatnik djelatnik,Odjel odjel);
    Radina getRadinaByIddjelatnik(Integer id);
    Set<Radina> getRadinasByIddjelatnik(Integer id);
    Radina findByIddjelatnik(Djelatnik djelatnik);


    @Transactional
    @Modifying
    @Query(value = "UPDATE Radina SET prosjecnovrijeme = :prosjecnovrijeme   WHERE sifodjel = :sifodjel AND iddjelatnik = :iddjelatnik", nativeQuery = true)
    void updateVrijeme(@Param("prosjecnovrijeme") Long prosjecnovrijeme, @Param("sifodjel") String sifodjel,@Param("iddjelatnik") Integer iddjelatnik);

    @Transactional
    @Modifying
    @Query(value = "UPDATE Radina SET brobradenih = :brobradenih   WHERE sifodjel = :sifodjel AND iddjelatnik = :iddjelatnik", nativeQuery = true)
    void updateBroj(@Param("brobradenih") Integer brobradenih,@Param("sifodjel") String sifodjel,@Param("iddjelatnik") Integer iddjelatnik);

}
