package MirkoISlavko.projekt.Controller;

import MirkoISlavko.projekt.domain.*;
import MirkoISlavko.projekt.service.DjelatnikService;
import MirkoISlavko.projekt.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

@CrossOrigin(origins={ "https://localhost:3000", "https://localhost:4200", "https://red-u-red-progi.herokuapp.com" })
@RestController
@RequestMapping("/djelatnik")
public class DjelatnikController {


    public static Map<Integer,String> djelatnikIodjel = new HashMap<>();
    public static Map<Integer, Long> djelatnikIstart = new HashMap();
    public static Map<Integer,String> djelatnikICookie = new HashMap<>();
    long finish;

    @Autowired
    private DjelatnikService djelatnikService;

    @Autowired
    private QueueService queueService;

    @CrossOrigin
    @PostMapping("")
    public Optional<Djelatnik> login(@RequestBody Djelatnik djelatnik) throws HttpMessageNotReadableException {
        return djelatnikService.login(djelatnik.getUsername());
    }

    @CrossOrigin
    @PostMapping("/{id}/firstLogin")
    public Integer firstLogin(@RequestBody Djelatnik djelatnik) {
        Integer id = djelatnikService.getIdOfDjelatnik(djelatnik.getUsername());
        djelatnikIodjel.put(id, "");
        djelatnikIstart.put(id, (long) 0);
        djelatnikICookie.put(djelatnik.getId(),"");
        djelatnikService.firstLogin(djelatnik.getUsername(), djelatnik.getLozinka());
        return djelatnikService.giveSalter(djelatnik.getUsername()).getId();
    }

    @CrossOrigin
    @PostMapping("/{id}/classicLogin")
    public Integer classicLogin(@RequestBody Djelatnik djelatnik) {
        Integer id = djelatnikService.getIdOfDjelatnik(djelatnik.getUsername());
        djelatnikIodjel.put(id, "");
        djelatnikIstart.put(id, (long) 0);
        djelatnikICookie.put(djelatnik.getId(),"");
        System.out.println(djelatnikIodjel);
        System.out.println(djelatnikIstart);
        Integer broj = 0;
        if (djelatnikService.classicLogin(djelatnik.getUsername(), djelatnik.getLozinka())) {
            broj = djelatnikService.giveSalter(djelatnik.getUsername()).getId();
        System.out.println(broj);
    }
        return broj;

    }

    @CrossOrigin
    @PostMapping("/{id}/logout")
    public void logout(@RequestBody Djelatnik djelatnik) {
        djelatnikIodjel.replace(djelatnik.getId(),"");
        djelatnikIstart.replace(djelatnik.getId(),(long)0);
        djelatnikICookie.replace(djelatnik.getId(),"");
        djelatnikService.logout(djelatnik.getUsername());
    }

    @CrossOrigin
    @GetMapping("/nadisve")
    public List<Djelatnik> nadisve() {
        return djelatnikService.listAll();
    }

    //n apoziv http zahtjeva djelatnika salje se
    // zahtjev i iz reda se pusta prvi koji posotji i
    // djelatnik njeg aobraduje sve dok se ne poalje novi
    // zahtjev
    //@PostMapping("/dodjeli")
    //public void dodjeli(@RequestBody Salter šalter) {
    //    queueService.add(šalter);
    //}

    @CrossOrigin
    @PostMapping("/vrijeme")
    public long vratiProsjecnovrijemeZaKlijenta(@RequestBody Request request) {
        System.out.println("REQUEST OD KATARINE" + request.getSalter() + " " + request.getCookie());
       // String cookie = djelatnikICookie.get(request.getCookie());
        Long vrijeme = queueService.getProsjecnovrijeme(request);
        if (vrijeme == null) {
            System.out.println("VRIJEME CEKANJA" + vrijeme);
            return (long)0;
        } else {
            System.out.println("VRIJEME CEKANJA" + vrijeme);
            return vrijeme;
        }
    }


    @CrossOrigin
    @PostMapping("/{id}/Queue")
    public Response stvoriRed(@RequestBody Djelatnik djelatnik) {
        if(djelatnikService.getQueueOfDjelatnik(djelatnik.getUsername()) == null)
           djelatnikService.makeQueue(djelatnik.getUsername());
        Integer iddjelatnik = djelatnikService.getIdOfDjelatnik(djelatnik.getUsername());
        Integer salter = djelatnikService.getSalterOfDjelatnik(iddjelatnik);
        Map<String,Long> pomocna_mapa = djelatnikService.getVrijemeOfOdjelZaDjelatnik(iddjelatnik);
        Response res = new Response();
        res.setSalter(salter);
        int i = 0;
        for(Map.Entry<String,Long> entry : pomocna_mapa.entrySet()) {
            if(i == 0) {
                res.setOdjelime1(entry.getKey());
                res.setOdjelvrijeme1(entry.getValue());
            } else {
                res.setOdjelime2(entry.getKey());
                res.setOdjelvrijeme2(entry.getValue());
            }
            i++;
        }
        res.setOdjelklijentS(djelatnikService.getOdjeliOfSlijedeciKlijent(iddjelatnik));
        return res;
        //return djelatnikService.getQueueOfDjelatnik(djelatnik.getUsername());
    }

    @CrossOrigin
    @PostMapping("/{id}/Queue/Next")
    public Response pomakniRed(@RequestBody Djelatnik djelatnik) {
        Integer idDjelatnik = djelatnikService.getIdOfDjelatnik(djelatnik.getUsername());
        long time = 0;
        System.out.println(djelatnikIstart);
        System.out.println(djelatnikIodjel);
        if(djelatnikIstart.get(idDjelatnik) == 0) {
            djelatnikIstart.replace(idDjelatnik,System.currentTimeMillis());
        } else {
            finish = System.currentTimeMillis();
            time = finish - djelatnikIstart.get(idDjelatnik);
            djelatnikIstart.replace(idDjelatnik,finish);
            String odjel = djelatnikIodjel.get(idDjelatnik);

            djelatnikService.putVrijemeInDataBase(time,odjel,idDjelatnik);
            }
        //MORAM VRATITI INFO O KLIJENTU KOJEG OVJDE POLLAm i njegovom trneutnom odjelu
        Klijent klijent = djelatnikService.getQueueOfDjelatnik(djelatnikService.getUsernameOfDjelatnik(idDjelatnik)).poll();
        //return djelatnikService.getQueueOfDjelatnik(djelatnik.getUsername());
        djelatnikIodjel.replace(idDjelatnik,klijent.getOdjelIBroj());
        // String sljedeciZadnjiOdjel =  djelatnikService.getOdjelOfQueue(djelatnik.getUsername());
        //djelatnikIodjel.replace(idDjelatnik,sljedeciZadnjiOdjel);
        djelatnikICookie.put(djelatnik.getId(),klijent.getCookie());
        Integer salter = djelatnikService.getSalterOfDjelatnik(idDjelatnik);
        Map<String,Long> pomocna_mapa = djelatnikService.getVrijemeOfOdjelZaDjelatnik(idDjelatnik);
        Response res = new Response();
        res.setSalter(salter);
        int i = 0;
        for(Map.Entry<String,Long> entry : pomocna_mapa.entrySet()) {
            if(i == 0) {
                res.setOdjelime1(entry.getKey());
                res.setOdjelvrijeme1(entry.getValue());
            } else {
                res.setOdjelime2(entry.getKey());
                res.setOdjelvrijeme2(entry.getValue());
            }
            i++;
        }
        res.setOdjelklijentS(djelatnikService.getOdjeliOfSlijedeciKlijent(idDjelatnik));
        res.setOdjelklijentT(djelatnikService.getOpisOdjela(klijent.getOdjelIBroj()));

        return res;

    }

}