package MirkoISlavko.projekt.Controller;


import MirkoISlavko.projekt.domain.Klijent;
import MirkoISlavko.projekt.domain.Response;
import MirkoISlavko.projekt.service.DjelatnikService;
import MirkoISlavko.projekt.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

@CrossOrigin(origins={ "http://localhost:3000", "http://localhost:4200", "https://red-u-red-progi.herokuapp.com" })
@RestController
@RequestMapping("/klijent")
public class KlijentController {


    @Autowired
    DjelatnikService djelatnikService;
    @Autowired
    QueueService queueService;

    @CrossOrigin
    @PostMapping("/red")
    public Response addToQueue(@RequestBody Klijent klijent) throws Exception {
        klijent.setCookie(queueService.generateRandomPassword(7));
        Response res = new Response();
        Integer salter = queueService.addToQueue(klijent);
        res.setSalter(salter);
        res.setCookie(klijent.getCookie());
        return res;
    }



}
