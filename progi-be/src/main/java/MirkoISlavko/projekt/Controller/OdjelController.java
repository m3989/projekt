package MirkoISlavko.projekt.Controller;


import MirkoISlavko.projekt.domain.Odjel;
import MirkoISlavko.projekt.service.OdjelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins={ "https://localhost:3000", "https://localhost:4200", "https://red-u-red-progi.herokuapp.com" })
@RestController
@RequestMapping("/odjel")
public class OdjelController {

    @Autowired
    private OdjelService odjelService;

    @CrossOrigin
    @GetMapping("")
    public List<Odjel> getAll() {
        return odjelService.listAllOdjel();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public  String findOpis(@PathVariable String id){
        return odjelService.findOpis(id);
    }
}
