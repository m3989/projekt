package MirkoISlavko.projekt.Controller;

import MirkoISlavko.projekt.domain.Administrator;
import MirkoISlavko.projekt.domain.Djelatnik;
import MirkoISlavko.projekt.domain.Request;
import MirkoISlavko.projekt.service.AdminService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@CrossOrigin(origins={ "http://localhost:3000", "http://localhost:4200", "https://red-u-red-progi.herokuapp.com" })
@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private  AdminService adminService;

    @CrossOrigin
    @PostMapping("/prijava")
    public Administrator login(@RequestBody Administrator administrator) throws HttpMessageNotReadableException {
        //return "HEllo" + administrator.getUsername();
        return adminService.login(administrator);
    }

    @CrossOrigin
    @PostMapping("/regi")
    public Djelatnik registerDjealtnik(@RequestBody Request djealtnik) {
        return  adminService.putDjelatnikDB(djealtnik);
    }


}
